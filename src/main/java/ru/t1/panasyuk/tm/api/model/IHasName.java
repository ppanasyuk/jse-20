package ru.t1.panasyuk.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}